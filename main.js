// Modules
const { app, BrowserWindow, webContents, ipcMain} = require("electron");

let mainWindow, secondaryWindow;

// Create a new BrowserWindow when `app` is ready
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
    },
  });

  // Menu.setApplicationMenu(mainMenu);
  
  let wc = mainWindow.webContents;
  let ses = mainWindow.webContents.session;
  console.log(ses);

  mainWindow.webContents.openDevTools();

  // mainWindow.loadurl("http://google.com");
  mainWindow.loadURL('http://localhost:3000');

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

// Electron `app` is ready
app.on("ready", () => {
  console.log("app is ready");

  console.log(app.getPath("home"));
  createWindow();
});

// Quit when all windows are closed - (Not macOS - Darwin)
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});

// When app icon is clicked and app is running, (macOS) recreate the BrowserWindow
app.on("activate", () => {
  if (mainWindow === null) createWindow();
});
