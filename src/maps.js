import { width } from "@mui/system";
import { useContext, useEffect, useState } from "react";
import deviceContext from "./context/deviceContext";
import {
  Map,
  InfoWindow,
  Marker,
  GoogleApiWrapper,
  Polyline,
  Polygon,
  Circle
} from "google-maps-react";
import React, { Component } from "react";

const style = {
  width: "90%",
  height: "70%",
  padding: "20px",
  margin: "30px",
  alignSelf: "center",
  top: "10%",
  border: "7px solid black",
  borderRadius: "20px",
};
const containerStyle = {
  position: "relative",
  width: "100%",
  height: "100%",
  alignSelf: "center",
  display: "flex",
  // padding:"20px"
};
// const triangleCoords = [
//     {lat:17.3850, lng: 78.4867},
//     {lat: 22.5726, lng:  88.3639},

//   ];
const triangleCoords = [
  { lat: 42.577413233757134, lng: 86.2588406805254 },

  { lat: 42.559761282694204, lng: 85.65938633401201 },
  { lat: 42.227260961195775, lng: 85.99515009578938 },
];

export const MapContainer = (props) => {
  //     const arr=[<Polyline
  //       path={triangleCoords}
  //       strokeColor="#0000FF"
  //       strokeOpacity={0.8}
  //       strokeWeight={2} />
  // ,
  //     <Polygon
  //       paths={triangleCoords}
  //       strokeColor="#0000FF"
  //       strokeOpacity={0.8}
  //       strokeWeight={2}
  //       fillColor="#0000FF"
  //       fillOpacity={0.35}
  //     />]
  const [arr, setArr] = useState([]);

  const DeviceContext = useContext(deviceContext);
  const Load = () => {
    var polyarr = [];
    var polyarr2 = [];
    var i = 1;
    polyarr = DeviceContext.satdata.map((item) => {
      console.log(item);
      // <Polyline
      //   path={item}
      //   strokeColor="#0000FF"
      //   strokeOpacity={0.8}
      //   strokeWeight={2} />)
      return (
        <Polygon
          key={i++}
          paths={item}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2}
          fillColor="#0000FF"
          fillOpacity={0.35}
        />
      );
    });
    polyarr.pop(); // this will give error if removed .. last eleement is being nan;
    console.log("polyarr is", polyarr);
    setArr(polyarr);
  };
  const fcoord={ lat: 17.5888, lng: 78.5379 };
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Map
        containerStyle={containerStyle}
        google={props.google}
        zoom={5}
        initialCenter={{
          lat: 17.385,
          lng: 78.4867,
        }}
        style={style}
      >
        {arr}

        {/* <Polyline
          path={triangleCoords}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2} />
 
        <Polygon
          paths={triangleCoords}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2}
          fillColor="#0000FF"
          fillOpacity={0.35}
        /> */}

        <Marker onClick={props.onMarkerClick} name={"Current location"} />

        <InfoWindow onClose={props.onInfoWindowClose}>
          {/* <div>
              <h1>{this.state.selectedPlace.name}</h1>
            </div> */}
        </InfoWindow>
        {/* <Marker
          name={"Your position"}
          position={{ lat: 22.5726, lng: 22.5726 }}
          icon={{
            url: "./icon.png",
          }}
        /> */}

        {/* <Marker
    title={'The marker`s title will appear as a tooltip.'}
    name={'SOMA'}
    position={{ lat: 22.5726, lng: 88.3639 }}/> */}
        <Marker
          name={"Dolores park"}
          position={fcoord}
          label={"Emitter"}
          // draggable={true}
          icon={{
            url: require("./icon.png"),
            anchor: new props.google.maps.Point(32, 32),
          }}
        />
        <Marker />
        <Circle
        radius={21200}
        	

        center={fcoord}
        onMouseover={() => console.log('mouseover')}
        onClick={() => console.log('click')}
        onMouseout={() => console.log('mouseout')}
        strokeColor='transparent'
        strokeOpacity={0}
        strokeWeight={5}
        fillColor='#FF0000'
        fillOpacity={0.2}
      />
      </Map>
      <button onClick={Load}>Click</button>
    </div>
  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyCdLa17noIWCnmw_wzocWTj8Rvh1WUHS4k",
})(MapContainer);

// export default Maps;
