import React, { useReducer } from "react";
import DeviceReducer from './deviceReducer';
import DeviceContext from "./deviceContext";
import { createBreakpoints } from "@mui/system";
import {convertxyz} from "./convertxyz";


const DeviceState = (props) => {
    const initialState = {
        data: [],  
        emitter:[],
        satdata:[]      
    };

    const [state, dispatch] = useReducer(DeviceReducer, initialState);


    const setDevice = async (data) => {
        try {            
            console.log('setdevice called');
            console.log(data);
            const satdata=convertxyz(data);
            dispatch({
                type: 'setdevice',
                payload: { data, satdata }
            });
        } catch (err) {
            console.log('error occured');
        }
    };
    const setEmitter = async (data) => {
        try {            
            console.log('SETEMITTER!!!');
            dispatch({
                type: 'setemitter',
                payload: data,
            });
        } catch (err) {
            console.log('error occured');
        }
    };

    return (
        <DeviceContext.Provider value={{
            data: state.data,
            emitter:state.emitter,
            setDevice,
            setEmitter,
            satdata:state.satdata,
        }}>
            {props.children}
        </DeviceContext.Provider>
    );
};

export default DeviceState;

