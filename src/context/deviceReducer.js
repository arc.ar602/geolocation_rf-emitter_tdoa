
  
  export default (state, action) => {
    switch (action.type) {
      case 'setdevice':
        return {
          ...state,
          data: action.payload.data,
          satdata:action.payload.satdata,
        };
      case 'setemitter':
        return {
          ...state,
          emitter: action.payload
        };
      
      default:
        return state;
    }
  };