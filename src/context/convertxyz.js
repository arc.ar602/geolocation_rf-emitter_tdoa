import projector from "ecef-projector";
export const convertxyz = (data) => {
  console.log("the data is", data);
  let arr = [];
  const t = data.forEach((item) => {
    var sat1 = projector.unproject(
      item.S1_X * 1000,
      item.S1_Y * 1000,
      item.S1_Z * 1000
    );
    var sat2 = projector.unproject(
      item.S2_X * 1000,
      item.S2_Y * 1000,
      item.S2_Z * 1000
    );
    var sat3 = projector.unproject(
      item.S3_X * 1000,
      item.S3_Y * 1000,
      item.S3_Z * 1000
    );
    arr.push([{ lat: sat1[0], lng:sat1[1] },{ lat: sat2[0], lng:sat2[1] },{ lat: sat3[0], lng:sat3[1] }]);
  });
   return arr;
};
