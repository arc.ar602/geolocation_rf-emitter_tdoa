import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';
// S1_X": 343.587177,
//     "S1_Y": 5254.550321,
//     "S1_Z": 4809.362545,
//     "S2_X": 397.261077,
//     "S2_Y": 5233.781107,
//     "S2_Z": 4790.830166,
//     "S3_X": 369.567202,
//     "S3_Y": 5278.636114,
//     "S3_Z": 4773.941442
const columns = [
  { field: 'id', headerName: 'ID', width: 70 },

  {
    field: 'S1_X',    headerName: 'S1_X',    type: 'number'  
    },
  {
    field: 'S1_Y',    headerName: 'S1_Y',    type: 'number'  
    },
  {
    field: 'S1_Z',    headerName: 'S1_Z',    type: 'number'  
    },
  {
    field: 'S2_X',    headerName: 'S2_X',    type: 'number'  
    },
  {
    field: 'S2_Y',    headerName: 'S2_Y',    type: 'number'  
    },
  {
    field: 'S2_Z',    headerName: 'S2_Z',    type: 'number'  
    },
  {
    field: 'S3_X',    headerName: 'S3_X',    type: 'number'  
    },
  {
    field: 'S3_Y',    headerName: 'S3_Y',    type: 'number'  
    },
  {
    field: 'S3_Z',    headerName: 'S3_Z',    type: 'number'  
    },
  
];

const rows = [
  { id:1, S1_X: 1, S1_Y: 2,S1_Z:4,S2_X: 1, S2_Y: 2,S2_Z:4,S3_X: 1, S3_Y: 2,S3_Z:4 },
  {id:2, S1_X: 1, S1_Y: 2,S1_Z:4,S2_X: 1, S2_Y: 2,S2_Z:4,S3_X: 1, S3_Y: 2,S3_Z:4 },
  
];

export default function DataTable() {
  return (
    <div style={{ height: 400, width: '50%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={15}
        rowsPerPageOptions={[15]}
        checkboxSelection
      />
    </div>
  );
}