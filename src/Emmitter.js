import React, { Component, useContext, useState } from "react";
import XLSX from "xlsx";
import { make_cols } from "./MakeColumns";
import { SheetJSFT } from "./types";
import deviceContext from "./context/deviceContext";
import { DataGrid } from "@mui/x-data-grid";

const columns = [
  { field: "id", headerName: "ID", width: 10 },

  {
    field: "LATITUDE",
    headerName: "LATITUDE",
    type: "number",
    headerAlign: "center",
    flex: 1,
    align: "center",
  },
  {
    field: "LONGITUDE",
    headerName: "LONGITUDE",
    headerAlign: "center",
    type: "number",
    flex: 1,
    align: "center",
  },
  {
    field: "ALTITUDE",
    headerName: "ALTITUDE",
    headerAlign: "center",
    type: "number",
    flex: 1,
    align: "center",
  },
];

var rows = [
  {
    id: 1,
    LATITUDE: 1234,
    LONGITUDE: 4566,
    ALTITUDE: 543,
  },
];

const Emitter = () => {
  const [file, setFile] = useState({});
  const [data, setData] = useState([]);
  const DeviceContext = useContext(deviceContext);

  const handleChange = (e) => {
    const files = e.target.files;
    if (files && files[0]) setFile(files[0]);
  };

  const handleFile = () => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, {
        type: rABS ? "binary" : "array",
        bookVBA: true,
      });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws);
      var i = 1;
      var p = data;
      p.forEach((item, i) => {
        item.id = i + 1;
      });

      console.log(p);
      rows = p;
      setData(p);
      DeviceContext.setEmitter(p);
    };

    if (rABS) {
      reader.readAsBinaryString(file);
    } else {
      reader.readAsArrayBuffer(file);
    }
  };

  return (
    <div>
      <div
        style={{
          height: "20vh",
          width: "100%",
          marginTop: "20px",
          padding: "20px",
        }}
      >
        <DataGrid rowHeight={70} rows={rows} columns={columns} pageSize={1} />
        <label htmlFor="file">Upload an excel to Process Triggers</label>
        <input
          type="file"
          className="form-control"
          id="file"
          accept={SheetJSFT}
          onChange={handleChange}
        />
        <input type="submit" value="Process Triggers" onClick={handleFile} />
      </div>
      {/* <Component1 data={this.state.data} /> */}
    </div>
  );
};

export default Emitter;
