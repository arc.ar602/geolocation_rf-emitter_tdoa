import React, { Component, useContext, useState } from "react";
import XLSX from "xlsx";
import { make_cols } from "./MakeColumns";
import { SheetJSFT } from "./types";
import deviceContext from "./context/deviceContext";
import { DataGrid } from "@mui/x-data-grid";

const columns = [
  { field: "id", headerName: "ID", width: 30 },
  {
    field: "Elevation Angle",
    headerName: "Elevation Angle",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S1_X",
    headerName: "S1_X",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S1_Y",
    headerName: "S1_Y",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S1_Z",
    headerName: "S1_Z",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S2_X",
    headerName: "S2_X",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S2_Y",
    headerName: "S2_Y",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S2_Z",
    headerName: "S2_Z",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S3_X",
    headerName: "S3_X",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S3_Y",
    headerName: "S3_Y",
    type: "number",
    minWidth: 30,
  },
  {
    field: "S3_Z",
    headerName: "S3_Z",
    type: "number",
    minWidth: 30,
  },
];

var rows = [
  {
    id: 1,
    S1_X: 1,
    S1_Y: 2,
    S1_Z: 4,
    S2_X: 1,
    S2_Y: 2,
    S2_Z: 4,
    S3_X: 1,
    S3_Y: 2,
    S3_Z: 4,
  },
  {
    id: 2,
    S1_X: 1,
    S1_Y: 2,
    S1_Z: 4,
    S2_X: 1,
    S2_Y: 2,
    S2_Z: 4,
    S3_X: 1,
    S3_Y: 2,
    S3_Z: 4,
  },
];

const ExcelReader = () => {
  const [file, setFile] = useState({});
  const [data, setData] = useState([]);
  const DeviceContext=useContext(deviceContext);

  const handleChange = (e) => {
    const files = e.target.files;
    if (files && files[0]) setFile(files[0]);
  };

  const handleFile = () => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, {
        type: rABS ? "binary" : "array",
        bookVBA: true,
      });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws);
      var i = 1;
      var p = data;
      p.forEach((item, i) => {
        item.id = i + 1;
      });

      console.log(p);
      rows = p;
      setData(p);
      DeviceContext.setDevice(p);
      
    };

    if (rABS) {
      reader.readAsBinaryString(file);
    } else {
      reader.readAsArrayBuffer(file);
    }
  };

  return (
    <div>
      <div style={{ height: "65vh", width: "100%" ,padding:"15px"}}>
        <DataGrid
        rowHeight={33}
          rows={rows}
          columns={columns}
          pageSize={15}
          rowsPerPageOptions={[15]}
        />
      <label htmlFor="file">Upload an excel to Process Triggers</label>
      
      <input
        type="file"
        className="form-control"
        id="file"
        accept={SheetJSFT}
        onChange={handleChange}
      />     
      <input type="submit" value="Process Triggers" onClick={handleFile} />
      </div>
      {/* <Component1 data={this.state.data} /> */}
    </div>
  );
};



// class ExcelReader extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       file: {},
//       data: [],
//       cols: [],
//     };
//     this.handleFile = this.handleFile.bind(this);
//     this.handleChange = this.handleChange.bind(this);
//   }

//   handleChange(e) {
//     const files = e.target.files;
//     if (files && files[0]) this.setState({ file: files[0] });
//   }

//   handleFile() {
//     /* Boilerplate to set up FileReader */
//     const reader = new FileReader();
//     const rABS = !!reader.readAsBinaryString;

//     reader.onload = (e) => {
//       /* Parse data */
//       const bstr = e.target.result;
//       const wb = XLSX.read(bstr, {
//         type: rABS ? "binary" : "array",
//         bookVBA: true,
//       });
//       /* Get first worksheet */
//       const wsname = wb.SheetNames[0];
//       const ws = wb.Sheets[wsname];
//       /* Convert array of arrays */
//       const data = XLSX.utils.sheet_to_json(ws);
//       var i=1;
//         var p=data;
//         p.forEach((item, i) => {
//           item.id = i + 1;
//         });

//         console.log(p);
//         rows=p;
//       /* Update state */
//       this.setState({ data: data, cols: make_cols(ws["!ref"]) }, () => {
//         // console.log(this.state.data);
//         // var i=1;
//         // var p=this.state.data;
//         // p.forEach((item, i) => {
//         //   item.id = i + 1;
//         // });

//         // console.log(p);
//         // rows=p;

//       });
//       this.setState();
//     };

//     if (rABS) {
//       reader.readAsBinaryString(this.state.file);
//     } else {
//       reader.readAsArrayBuffer(this.state.file);
//     }
//   }

//   render() {
//     return (
//       <div>
//         <div style={{ height: "70vh", width: "100%" }}>
//           <DataGrid
//             rows={rows}
//             columns={columns}
//             pageSize={15}
//             rowsPerPageOptions={[15]}
//           />
//         </div>
//         <label htmlFor="file">Upload an excel to Process Triggers</label>
//         <br />
//         <input
//           type="file"
//           className="form-control"
//           id="file"
//           accept={SheetJSFT}
//           onChange={this.handleChange}
//         />
//         <br />
//         <input
//           type="submit"
//           value="Process Triggers"
//           onClick={this.handleFile}
//         />
//         {/* <Component1 data={this.state.data} /> */}
//       </div>
//     );
//   }
// }

export default ExcelReader;
