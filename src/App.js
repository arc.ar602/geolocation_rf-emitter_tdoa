import "./App.css";
import { useEffect } from "react";
import ExcelReader from "./ExcelReader";
import Emitter from "./Emmitter";
import { Table } from "@mui/material";
import DataTable from "./table";
import DeviceState from "./context/deviceState";
import Box from "@mui/material/Box";
import MapContainer from "./maps"
function App() {
  return (
    <DeviceState>
      <div className="App">
        <Box sx={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)" }}>
          <Box sx={{ flexDirection: "column", }}>
            <ExcelReader />

            <Emitter />
          </Box>
          <MapContainer/>
        </Box>
      </div>
    </DeviceState>
  );
}

export default App;
